# README #

Simple blog created with the MEAN stack (MongoDB, Express, Angular, Node).

The idea is to get to know new Javascript technologies for both frontend and backend.
Will be built into a single Docker container for easy deployment anywhere, stay tuned for more!


### Tools used for development###

* Gulp
* Jade
* Sass
* Stylus (coming up)

### How do I get set up? ###

* TBD



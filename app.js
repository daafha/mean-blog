(function (){
	
'use strict';

// declare modules
angular.module('Authentication', []);
angular.module('Home', []);
angular.module('Default', []);


angular.module('BlogApp', [
    'Authentication',
    'Home',
    'ngRoute',
    'ngCookies',
    'Default'
])

	.config(['$routeProvider', '$locationProvider', function ($routeProvider, $locationProvider) {
 
    $routeProvider

		// home page
		.when('/', {
			templateUrl: 'homepage.html',
			controller: 'BlogController'
		})

		.when('/post', {
			templateUrl: 'modules/post/views/post.html',
			controller: 'HomeController'
		})

		.when('/login', {
			templateUrl: 'modules/authentication/views/login.html',
			controller: 'LoginController'	
		});

	$locationProvider.html5Mode(true);
}])
.run(['$rootScope', '$location', '$cookieStore', '$http',
    function ($rootScope, $location, $cookieStore, $http) {
        // keep user logged in after page refresh
        $rootScope.globals = $cookieStore.get('globals') || {};
        if ($rootScope.globals.currentUser) {
            $http.defaults.headers.common['Authorization'] = 'Basic ' + $rootScope.globals.currentUser.authdata; // jshint ignore:line
        }
 
        $rootScope.$on('$locationChangeStart', function (event, next, current) {
            // redirect to login page if not logged in
            if ($location.path() == '/post' && !$rootScope.globals.currentUser) {
                $location.path('/login');
            }
        });
    }]);

})
();
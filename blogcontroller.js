'use strict';
 
angular.module('Default')
 
.controller("BlogController", BlogController);


	function BlogController($scope,$http,$sce){
		$scope.createPost = createPost;
		$scope.nextPost = nextPost;
		function init(){
			getAllPosts();
		}
		init();

		function getAllPosts (){
			$http.
			get("/rest/blogpost")
			.success(function(posts){
				console.log(posts[0].posted);
				var date = new Date(posts[0].posted);
					date = 'Posted: '+date.getDate() + '.'+(date.getMonth()+1)+'.' + date.getFullYear();//prints expected format				
					$scope.time =  $sce.trustAsHtml("<b>"+ date + "</b>");
					$scope.category = posts[0].category;
					console.log("categoria:"+posts[0].category);
					$scope.title = posts[0].title;
					console.log($scope.time);
					var text = posts[0].body;
					text = text.replace(/\r?\n/g, '<br />');
					$scope.trustedbody = $sce.trustAsHtml(text);

				})
		}

		function createPost(post) {
			console.log("createPost in action!");
			console.log(post);
			$http
			.post("/rest/blogpost", post)
			.success(getAllPosts);	
			
		}

		function nextPost() {
			console.log("nextPost is activated!");
			var count = $scope.count;
			console.log(count);
			var config = {
				params: {
					goto: count,
				}
			}

			$http.
			get("/rest/nextpost", config)
			.success(function(posts){
				posts = posts[count];
				console.log(posts);
				var date = new Date(posts.posted);
					date = 'Posted: '+date.getDate() + '.'+(date.getMonth()+1)+'.' + date.getFullYear();//prints expected format				
					$scope.time =  $sce.trustAsHtml("<b>"+ date + "</b>");
					$scope.posts = posts;
					$scope.category = posts.category;
					
					$scope.title = posts.title;
					console.log($scope.time);
					var text = posts.body;
					text = text.replace(/\r?\n/g, '<br />');
					$scope.trustedbody = $sce.trustAsHtml(text);

				})
			


			
		}

	}
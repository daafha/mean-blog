var gulp = require('gulp');
var autoprefixer = require('autoprefixer');
var browserSync = require('browser-sync');
var cleanCSS = require('gulp-clean-css');
var concat = require('gulp-concat');
var del = require('del');
var imagemin = require('gulp-imagemin');
var postcss = require('gulp-postcss');
var sass = require('gulp-sass');
var streamqueue = require('streamqueue');
var uglify = require('gulp-uglify');
var express = require('express');
var jade = require('gulp-jade');
var tinylr = require('tiny-lr');
var app = express();
var server = tinylr();
var path = require('path');
var gutil = require('gulp-util');
var reload = browserSync.reload;
var nodemon = require('gulp-nodemon');

/**
 * Gulp Tasks
 */
 
gulp.task('clean', function () {
    del.sync(['images', 'js']);
});

gulp.task('images', function () {
    return gulp.src('src/images/**/*')
        .pipe(gulp.dest('images'))

});

gulp.task('js', function () {
    // Copy in vendor scripts
    gulp.src('src/js/vendor/**/*.js')
        .pipe(gulp.dest('js/vendor'));

    
});


gulp.task('css', function () {
    // To concat CSS and SASS, we need to merge two separate streams in the
    // right order (hence streamqueue).
    var sassStream = gulp.src('src/sass/style.scss')
        // Don't exit on errors
        .pipe(sass()).on('error', function (error) { console.log(error.toString()); this.emit('end'); })
        .pipe(postcss([autoprefixer]));

    var cssStream = gulp.src('src/css/vendor/normalize.css');

    return streamqueue({objectMode: true}, cssStream, sassStream)
        .pipe(concat('style.css'))
        .pipe(cleanCSS({compatibility: 'ie8', keepSpecialComments: '*'}))
        .pipe(gulp.dest(''))
   
});

gulp.task('browser-sync', ['nodemon'], function() {
  browserSync({
    proxy: "localhost:4000",  // local node app address
    port: 5000,  // use *different* port than above
    notify: true
  });
});

gulp.task('nodemon', function (cb) {
  var called = false;
  return nodemon({
    script: 'app.js',
    script: 'server.js',
    ignore: [
      'gulpfile.js',
      'node_modules/'
    ]
  })
  .on('start', function () {
    if (!called) {
      called = true;
      cb();
    }
  })
  .on('restart', function () {
    setTimeout(function () {
      reload({ stream: false });
    }, 1000);
  });
});

gulp.task('default', ['images','js','css','browser-sync'], function () {
gulp.watch('src/sass/*.scss',['css']).on('change', browserSync.reload);
gulp.watch('*.js',['js']).on('change', browserSync.reload);
gulp.watch('modules/authentication/*.js',['js']).on('change', browserSync.reload);
gulp.watch('modules/post/*.js',['js']).on('change', browserSync.reload);
gulp.watch('modules/authentication/views/*.html').on('change', browserSync.reload);
gulp.watch('modules/post/views/*.html').on('change', browserSync.reload);
gulp.watch('src/images/**/*',['images']).on('change', browserSync.reload);
gulp.watch('*.html').on('change', browserSync.reload);

});
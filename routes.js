module.exports = function(app) {

	// server routes ===========================================================
	// handle things like api calls
	// authentication routes
	//TODO: Remove model and add it to a module of its own

// MONGO SCHEMA ==================================================

var PostSchema = mongoose.Schema({
  title: {type:String,required:true},
  body: {type:String,required:true},
  category: {type:String, enum:['APPSEC', 'CYBER DEFENSE','IAM','MSS'], required:true},
  posted: {type:Date,default:Date.now}


},

{collection:'post'});
var PostModel = mongoose.model("PostModel", PostSchema);

// FOR CREATING NEW USERS IN THE FUTURE
var UserSchema = mongoose.Schema({
  username: {type:String,required:true},
  password: {type:String,required:true},
},

{collection:'users'});
var UserModel = mongoose.model("UserModel", UserSchema);


// API CALLS ==================================================

app.get('/rest/blogpost', getAllPosts);
  function getAllPosts(req,res){
    PostModel
      .find().limit(1).sort({$natural:-1})
      .then(
          function (posts){
            res.json(posts);
          },
          function (err){
            res.sendStatus(400);
          }
        );

  }

  //HASHING THE PASSWORD -- CREDIT TO SIMPPA
  var sha1 = require('sha1');


 app.post('/rest/authenticate', auth);
  function auth(req,res){
  	var user = req.body.username;
  	var pass = req.body.password;
    //dev->remove
    console.log("plaintext password:" + pass )
    //Use npm sha1 lirbary for generating the hash
    var hashpass = sha1(pass);
    //dev->remove
    console.log("hashed password:" + hashpass )

    UserModel
      .find({$and:[{"username" : user, "password": hashpass }]}).limit(1)
      .then(
          function (users){
            res.json(users);
          },
          function (err){
            res.sendStatus(400);
          }
        );

  }

//for getting next post object
app.get('/rest/nextpost', getNextPosts);
  function getNextPosts(req,res){
    var next = req.query.goto;
    PostModel
      .find().sort({$natural:-1})
      .then(
          function (posts){
            res.json(posts);
          },
          function (err){
            res.sendStatus(400);
          }
        );

  } 


app.post("/rest/blogpost", createPost);

function createPost(req,res){
  var post = req.body;
  console.log(post);
  PostModel
  .create(post)
  .then(
    function(postObj){
      res.json(200);

    },
    function(error){
      res.sendStatus(400);
    }
    );
}

	// frontend routes =========================================================
	// route to handle all angular requests
	app.get('*', function(req, res) {
		res.sendfile('default.html');
	});

};
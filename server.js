var express = require('express')
, logger = require('morgan')
, app = express()
, path = require('path')
, bodyParser = require('body-parser');
mongoose = require('mongoose');
mongoose.connect('mongodb://127.0.0.1/blog');
app.use(express.static(path.resolve('')))
app.use(logger('dev'))
app.use(bodyParser.json()); // for parsing application/json
app.use(bodyParser.urlencoded({ extended: true })); 

var port = process.env.PORT || 4000; // set our port




// ROUTES ==================================================

require('./routes')(app); // pass our application into our routes

// START APPLICATION ====================================

app.listen(port); 
console.log('Listening on localhost port:' + port);       // shoutout to the user
exports = module.exports = app; 